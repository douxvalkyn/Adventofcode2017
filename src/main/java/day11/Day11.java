package day11;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import outils.Outil;

public class Day11 {

	// main -----
	public static void main(String[] args) throws IOException  {
		System.out.println("["+Day11.class.getSimpleName()+"]");
		Day11 day = new Day11();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		//day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}


	//https://www.redblobgames.com/grids/hexagons/

	
	
	// run1 ----
		public void run1() throws IOException {
	//String input="ne,ne,ne";
	//String input="ne,ne,sw,sw";
	//String input="ne,ne,s,s";
	//String input="se,sw,se,sw,sw";
	String filename = "resources/day11.txt";
	String input=Outil.importationString(filename).get(0);
	
	//lecture input
	String[] trajet = StringUtils.split(input,",");
	int coordQ = 0;	
	int coordR = 0;	
	int coordS = 0;	
	
	int distMax = 0;
	
	for (String deplacement: trajet) {
		if (deplacement.equals("n")) {
			coordS++;
			coordR--;
		}
		if (deplacement.equals("nw")) {
			coordS++;
			coordQ--;
		}
		if (deplacement.equals("sw")) {
			coordR++;
			coordQ--;
		}
		if (deplacement.equals("s")) {
			coordR++;
			coordS--;
		}
		if (deplacement.equals("se")) {
			coordQ++;
			coordS--;
		}
		if (deplacement.equals("ne")) {
			coordQ++;
			coordR--;
		}
		
		int dist = Math.max(Math.abs(coordS), Math.max(Math.abs(coordS), Math.abs(coordQ)));
		if (dist>distMax) {distMax=dist;}
	}
//	System.out.println(coordQ);
//	System.out.println(coordR);
//	System.out.println(coordS);
	
	//calcul de la distance= plus grande valeur entre q, r, s
	int dist = Math.max(Math.abs(coordS), Math.max(Math.abs(coordS), Math.abs(coordQ)));
		System.out.println("dist: "+dist);
		System.out.println("distMax: " + distMax);
			
			
		}
	


}
